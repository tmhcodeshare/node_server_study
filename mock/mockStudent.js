const Mock = require("mockjs");
const result = Mock.mock({
  "datas|500-700": [
    {
      name: "@cname",
      birthday: "@date",
      "sex|0-1": 0,
      tel: /1\d{10}/,
      "classId|1-16": 0,
    },
  ],
}).datas;
// console.log(result);
const Student = require("../models/Student");
Student.bulkCreate(result);
