const fs = require("fs");
const path = require("path");
function getStaticSource(path, req, res, next) {
  const rs = fs.createReadStream(path);
  rs.pipe(res);
}
// 服务器资源请求处理中间件
module.exports = (req, res, next) => {
  // 文件资源路径处理
  const filename = path.join(
    __dirname,
    "../resources",
    req.url.replace(/^\/resources/, "")
  );
  console.log(`resourcesMiddleware: ${filename}`);
  getStaticSource(filename, req, res, next);
};
