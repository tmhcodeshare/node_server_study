// module.exports = (req, res, next) => {
//   if (req.path.startsWith("/api")) {
//     // 说明你请求的是 api 接口
//     next();
//   } else {
//     // 说明你想要的是静态资源
//     if (true) {
//       res.send("静态资源");
//     } else {
//       next();
//     }
//   }
// };

const fs = require("fs");
const path = require("path");
function getStaticSource(path, req, res, next) {
  const rs = fs.createReadStream(path);
  if (!path.endsWith(".html")) {
    // 强缓存
    res.set("Cache-Control", `max-age=${3600 * 24 * 30}`);
  }
  rs.pipe(res);
}
// 服务器静态资源中间件
module.exports = function staticMiddleware(staticPath) {
  return (req, res, next) => {
    // console.log("requesturl", req.url);
    if (req.url.startsWith("/api")) {
      // 说明你请求的是 api 接口
      next();
    } else if (req.url.startsWith("/resources")) {
      // 文件资源路径处理
      next();
    } else {
      if (req.url === "/") {
        req.url = "/index.html";
      }
      // console.log("staticPath", staticPath);
      const filename = path.join(staticPath, req.url);
      // console.log(`staticMiddleware: ${filename}`);
      getStaticSource(filename, req, res, next);
    }
  };
};
