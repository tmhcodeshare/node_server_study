const express = require("express");
const path = require("path");
// 创建实例
const app = express();

// 应用中间件

// cors中间件
// 手写cors中间件
// app.use(require("./corsMiddleware"));
const cors = require("cors");
// const whitelist = ["http://localhost:5500", "http://localhost:8080"];
app.use(
  cors({
    origin: function (origin, callback) {
      // if (whitelist.indexOf(origin) !== -1) {
      //   callback(null, true);
      // } else {
      //   callback(new Error("Not allowed by CORS"));
      // }
      // 允许所有请求origin
      callback(null, true);
    },
    credentials: true,
  })
);

// 代理转发请求中间件
// 手写
const context = "/data";
const target = "http://localhost:3000";
const proxyMiddleware = require("./proxyMiddleware");
app.use(
  context,
  proxyMiddleware({
    target,
    pathRewrite: function (path, req) {
      console.log(path);
      return path;
    },
  })
);
// const { createProxyMiddleware } = require("http-proxy-middleware");
// const target = "http://localhost:3000";
// app.use(
//   context,
//   createProxyMiddleware({
//     target: target,
//     pathRewrite: function (path, req) {
//       console.log(path)
//       return path
//     },
//   })
// );

// 图片防盗中间件
app.use(require("./imgProtectMiddleware"));

// 1.静态资源中间件
const staticPath = path.join(__dirname, "../public");
console.log("staticPath", staticPath);
// app.use(
//   express.static(staticPath, {
//     setHeaders(res, path) {
//       if (!path.endsWith(".html")) {
//         // 强缓存
//         res.set("Cache-Control", `max-age=${3600 * 24 * 30}`);
//       }
//     },
//   })
// );
// 手写静态资源中间件
const staticMiddleware = require("./staticMiddleware");
app.use(staticMiddleware(staticPath));

// cookie中间件
// const cookieParser = require("cookie-parser");
// app.use(cookieParser());

// authorization中间件
app.use(require("./authorMiddleware"));

// 解析 application/x-www-form-urlencoded 格式的请求体
app.use(express.urlencoded({ extended: true }));

// 解析 application/json 格式的请求体解析
app.use(express.json());

// apiLogger中间件
app.use(require("./apiLogMiddleware"));

// 路由中间件 参数一为前缀，匹配到该前缀的请求才会进入该路由中间件
// router本质也是一个中间件，只是它内部封装了路由规则，并且返回一个函数
// const studentRouter = require("./api/studentRouter");
// console.log("studentRouter", typeof studentRouter);
app.use("/api/student", require("./api/studentRouter"));
app.use("/api/class", require("./api/classRouter"));
app.use("/api/book", require("./api/bookRouter"));
app.use("/api/admin", require("./api/adminRouter"));
app.use("/api/upload", require("./api/uploadRouter"));
app.use("/api/download", require("./api/downloadRouter"));

// 服务器资源请求拦截中间件
app.use('/resources', require('./resourcesMiddleware'))

// 错误拦截中间件
app.use(require("./errorMiddleware"));
const port = 6061;
app.listen(port, () => {
  console.log(`server is running at http://localhost:${port}`);
});

// app.get("/api/test", (req, res) => {
//   console.log(req.query);
//   res.send("hello world");
// });

// app.post("/api/test", (req, res) => {
//   console.log(req.body);
//   res.send("hello world");
// });
