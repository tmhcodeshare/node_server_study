const { pathToRegexp }  = require("path-to-regexp");
module.exports = {
  "/api/admin": {
    list: [
      {
        path: pathToRegexp("/login"),
        method: "POST",
        needToken: false,
      },
      {
        path: pathToRegexp("/add"),
        method: "POST",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "DELETE",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "PUT",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "GET",
        needToken: true,
      },
      {
        path: pathToRegexp("/:page/:limit"),
        method: "GET",
        needToken: true,
      },
    ],
  },
  "/api/book": {
    list: [
      {
        path: pathToRegexp("/add"),
        method: "POST",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "DELETE",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "PUT",
        needToken: true,
      },
    ],
  },
  "/api/class": {
    list: [
      {
        path: pathToRegexp("/add"),
        method: "POST",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "DELETE",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "PUT",
        needToken: true,
      },
    ],
  },
  "/api/student": {
    list: [
      {
        path: pathToRegexp("/add"),
        method: "POST",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "DELETE",
        needToken: true,
      },
      {
        path: pathToRegexp("/:id"),
        method: "PUT",
        needToken: true,
      },
    ],
  },
};
