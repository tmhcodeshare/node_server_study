const express = require("express");
const router = express.Router();
const path = require("path");
router.get("/:filename", (req, res, next) => {
  const fileName = req.params.filename;
  const ext = path.extname(fileName);
  const filepath = path.join(
    __dirname,
    `../../resources/${ext.split(".")[1]}/${fileName}`
  );
  res.download(filepath, fileName, (err) => {
    if (err) {
      console.log(err);
    }
  });
});

module.exports = router;
