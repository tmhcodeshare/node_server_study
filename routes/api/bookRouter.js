const express = require("express");
const router = express.Router();
const bookService = require("../../services/bookService");
const { asyncHandler } = require("../../utils/routerHelper");
// 添加一本书
router.post(
  "/add",
  asyncHandler(async (req, res, next) => {
    return await bookService.addBook(req.body);
  })
);
// 删除一本书
router.delete(
  "/:id",
  asyncHandler(async (req, res, next) => {
    return await bookService.deleteBook(req.params.id);
  })
);
// 修改一本书
router.put(
  "/:id",
  asyncHandler(async (req, res, next) => {
    return await bookService.updateBook(req.params.id, req.body);
  })
);

// 分页查询书籍
router.get(
  "/:page/:limit",
  asyncHandler(async (req, res, next) => {
    return await bookService.getBooks(+req.params.page, +req.params.limit);
  })
);
// 按书名查询书籍
router.get(
  "/:name",
  asyncHandler(async (req, res, next) => {
    return await bookService.getBookByName(req.params.name);
  })
);
// 按书名模糊查询
router.post(
  "/name_like",
  asyncHandler(async (req, res, next) => {
    const { name, page, limit } = req.body;
    return await bookService.getBookByNameLike(
      name,
      page && +page,
      limit && +limit
    );
  })
);
// 按作者模糊查询
router.post(
  "/author",
  asyncHandler(async (req, res, next) => {
    const { name, page, limit } = req.body;
    return await bookService.getBookByAuthorNameLike(
      name,
      page && +page,
      limit && +limit
    );
  })
);
module.exports = router;
