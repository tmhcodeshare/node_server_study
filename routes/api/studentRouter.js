const express = require("express");
// 创建路由实例
const router = express.Router();
// 引入服务层
const studentService = require("../../services/studentService");
const { asyncHandler } = require("../../utils/routerHelper");
// 编写api
//添加学生
router.post(
  "/add",
  asyncHandler(async (req, res, next) => {
    // console.log("addstudent", req.body);
    const result = await studentService.addStudent(req.body);
    return result;
  })
);
// 删除学生
router.delete(
  "/:id",
  asyncHandler(async (req, res, next) => {
    const id = req.params.id;
    const result = await studentService.deleteStudent(+id);
    return result;
  })
);
// 修改学生
router.put(
  "/:id",
  asyncHandler(async (req, res, next) => {
    const id = req.params.id;
    const result = await studentService.updateStudent(+id, req.body);
    return result;
  })
);
// 获取所有学生列表(分页)
router.get(
  "/:page/:limit",
  asyncHandler(async (req, res, next) => {
    const { page, limit } = req.params;
    const result = await studentService.getStudents(+page, +limit);
    // console.log("getstudents", result);
    return result;
  })
);

// 根据id获取学生信息
router.get(
  "/:id",
  asyncHandler(async (req, res, next) => {
    const id = req.params.id;
    if (!id) {
      // console.log("id不存在");
      return "next";
    }
    const result = await studentService.getStudentById(+id);
    return result;
  })
);
// 根据tellephone获取学生信息
router.get(
  "/",
  asyncHandler(async (req, res, next) => {
    const { tel } = req.query;
    if (!tel) {
      return "next";
    }
    const result = await studentService.getStudentByTel(tel);
    return result;
  })
);
// 根据学生姓名获取学生信息
router.post(
  "/name",
  asyncHandler(async (req, res, next) => {
    const { name, page, limit } = req.body;
    // console.log("name", name);
    const result = await studentService.getStudentByName(
      name,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
// 根据classId分页查询学生列表
router.post(
  "/class_id",
  asyncHandler(async (req, res, next) => {
    // console.log("class_id", req.query);
    const { classId, page, limit } = req.body;
    const result = await studentService.getStudentByClassId(
      +classId,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
//  电话模糊匹配分页查询学生列表
router.post(
  "/tel_like",
  asyncHandler(async (req, res, next) => {
    const { tel, page, limit } = req.body;
    const result = await studentService.getStudentByTelLike(
      tel_like,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
// 查询年龄大于指定年龄的学生列表
router.post(
  "/over_age",
  asyncHandler(async (req, res, next) => {
    const { age, page, limit } = req.body;
    const result = await studentService.getStudentByOverAge(
      +age,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
// 查询年龄小于指定年龄的学生列表
router.post(
  "/under_age",
  asyncHandler(async (req, res, next) => {
    const { age, page, limit } = req.body;
    const result = await studentService.getStudentByUnderAge(
      +age,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
// 查询年龄在指定年龄区间的学生列表
router.post(
  "/between_age",
  asyncHandler(async (req, res, next) => {
    const { startAge, endAge, page, limit } = req.body;
    const result = await studentService.getStudentByRangeAge(
      +startAge,
      +endAge,
      page && +page,
      limit && +limit
    );
    return result;
  })
);
// 联表查询(班级加学生)
router.get(
  "/info/:page/:limit",
  asyncHandler(async (req, res, next) => {
    const { page, limit } = req.params;
    const result = await studentService.getCompleteStudentInfoList(
      page && +page,
      limit && +limit
    );
    return result;
  })
);
module.exports = router;
