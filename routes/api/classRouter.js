const express = require('express');
// 创建路由实例
const router = express.Router();
// 引入服务层
const classService = require('../../services/classService');
const { asyncHandler }  = require('../../utils/routerHelper')
// 添加一个班级
router.post(
  '/add',
  asyncHandler(async (req, res, next) => {
    console.log(req.body)
    const result = await classService.addClass(req.body);
    return result
  })
)
// 删除一个班级
router.delete(
  "/:id",
  asyncHandler(async (req, res, next) => {
    const result = await classService.deleteClass(+req.params.id);
    return result;
  })
)
// 修改一个班级
router.put(
  "/:id",
  asyncHandler(async (req, res, next) => {
    const { id } = req.params;
    const body = req.body;
    const result = await classService.updateClass(+id, body);
    return result;
  })
)
// 根据班级名称获取班级信息
router.get(
  "/:name",
  asyncHandler(async (req, res, next) => {
    const result = await classService.getClasseByName(req.params.name);
    return result;
  })
)
// 分页获取班级列表
router.get(
  "/:page/:limit",
  asyncHandler(async (req, res, next) => {
    const { page, limit } = req.params;
    const result = await classService.getClasses(+page, +limit);
    return result;
  })
)
module.exports = router;