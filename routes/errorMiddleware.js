// 处理错误的中间件

module.exports = (err, req, res, next) => {
  console.log("err", err);
  if (err) {
    const errObj = err instanceof Error ? err.message : err;
    //发生了错误
    res.status(500).send(errObj);
  } else {
    next();
  }
};
