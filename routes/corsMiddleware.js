// cors跨域请求处理中间件
module.exports = function (req, res, next) {
  // console.log("origin", req.headers);
  // 复杂请求(预检请求)处理
  if (req.method === "OPTIONS") {
    console.log("预检请求");
    res.header(
      "Access-Control-Allow-Methods",
      req.headers["access-control-request-method"]
    );
    res.header(
      "Access-Control-Allow-Headers",
      req.headers["access-control-request-headers"]
    );
  }
  // 身份验证请求处理
  res.header("Access-Control-Allow-Credentials", true);
  // 简单请求cors处理 只需要设置Access-Control-Allow-Origin响应头即可
  if ("origin" in req.headers && req.headers.origin) {
    // console.log("origin", req.headers.origin);
    res.header("Access-Control-Allow-Origin", req.headers.origin);
    // req.header('Access-Control-Allow-Credentials', true);
  }
  next();
};
