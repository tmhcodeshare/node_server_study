const path = require("path");
// 通过判断 referer 来判断该图片请求是否是跨站请求，如果是则拦截并返回 logo.jpeg
// 如果不是则移交后续处理
module.exports = (req, res, next) => {
  const host = req.headers.host;
  const referer = req.headers.referer;
  const extname = path.extname(req.url);
  if ([".png", ".jpg", ".jpeg", ".gif", ".svg"].includes(extname)) {
    console.log("host", host);
    console.log("referer", referer);
    if (!referer || (referer && referer.includes(host))) {
      next();
    } else {
      // res.status(403).send("Forbidden");
      req.url = "/resources/jpeg/logo.jpeg";
      console.log("referer is not include host");
      next();
      // res.status(403).sendFile(path.join(__dirname, "../public/403.html"));
    }
  } else {
    next();
  }
};
