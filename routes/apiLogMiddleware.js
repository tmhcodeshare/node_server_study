const { apiLogger } = require("../logger");
const log4js = require("log4js");
// console.log("log4js", log4js);
// 链接api日志
module.exports = log4js.connectLogger(apiLogger, {
  level: "auto",
});
