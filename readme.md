## node微型服务器
### 集成
- express 路由层
- sequelize orm框架 + mysql2驱动 服务层
- validate.js 服务层校验
- mysql 数据库
- jsonwebtoken 身份验证
- multer 文件上
- cors 跨域
- log4js 日志

### 使用
- pnpm i
- npm start

### 路由
- /api/admin
  - /login 登录
- /api/upload
  - /file 单个文件上传
  - /multiple 多个文件上传
  - /slice 分片上传
  - /merge 分片合并
......
### 功能
- 管理员登录
- 添加管理员
- 获取管理员列表
- 删除管理员
- 修改管理员信息
- 上传文件
- 下载文件
- 大文件分片上传/分片合并
......

- 更多内容自行阅读