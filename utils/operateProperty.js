/**
 * 根据指定的属性过滤掉传入对象的一些属性
 * @param {*} object 需要过滤属性的对象
 * @param {*} property 指定过滤的属性
 * @returns
 */
exports.filterProperty = (object, ...property) => {
  if (!object) {
    return object;
  }
  property = property.flat();
  // console.log('property',property)
  // 过滤掉指定属性
  const obj = {};
  for (const key in object) {
    if (!property.includes(key)) {
      // obj[key] = object[key];
      if (object[key] && typeof object[key] === "object") {
        obj[key] = exports.filterProperty(object[key], ...property);
      } else {
        obj[key] = object[key];
      }
    }
  }
  // console.log('obj',obj)
  return obj;
};

/**
 * 根据指定的属性列表筛选出传入对象的一些属性
 * @param {*} object 需要筛选属性的对象
 * @param  {...any} property 指定筛选的属性
 * @returns
 */
exports.pickerProperty = (object, ...property) => {
  property = property.flat();
  // console.log('property',property)
  // 筛选出指定属性
  const obj = {};
  for (const key in object) {
    if (property.includes(key)) {
      obj[key] = object[key];
    }
  }
  return obj;
};

/**
 * 深拷贝对象
 * @param {*} obj 需要深拷贝的对象
 * @returns
 */
const deepClone = (obj) => {
  // 处理 null 和 undefined
  if (obj === null || typeof obj !== "object") {
    return obj;
  }

  // 处理 Date
  if (obj instanceof Date) {
    return new Date(obj.getTime());
  }

  // 处理 RegExp
  if (obj instanceof RegExp) {
    return new RegExp(obj);
  }

  // 处理数组
  if (Array.isArray(obj)) {
    const arrCopy = [];
    for (let i = 0; i < obj.length; i++) {
      arrCopy[i] = deepClone(obj[i]);
    }
    return arrCopy;
  }

  // 处理对象
  const objCopy = {};
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      objCopy[key] = deepClone(obj[key]);
    }
  }

  return objCopy;
};

exports.deepClone = deepClone;
