exports.getErrorMessage = (error = "server internal error", errrCode = 500) => {
  return {
    code: errrCode,
    msg: error,
    data: null,
  };
};

exports.getSuccessMessage = (data = null) => {
  return {
    code: 0,
    msg: "success",
    data: data,
  };
};

exports.asyncHandler = (callback) => {
  // 翻译一个async函数，统一处理错误捕获
  return async (req, res, next) => {
    try {
      const result = await callback(req, res, next);
      if (result === "next") {
        // console.log("result", result);
        // 移交下一个匹配规则
        next();
      } else {
        // console.log("继续执行了");
        res.status(200).json(exports.getSuccessMessage(result));
      }
    } catch (error) {
      console.log("error", error);
      next(exports.getErrorMessage(error.message, error.code || 500));
    }
  };
};

