const jwt = require("jsonwebtoken");

const algorithm = "HS256";
const seceret = "acdv1pu6q0xm6k1t";

const encryptToken = (payload) => {
  return jwt.sign(payload, seceret, {
    algorithm: algorithm,
    expiresIn: 60 * 60 * 24 * 1000,
  });
};

const decryptToken = (token) => {
  try {
    return jwt.verify(token, seceret, {
      algorithm: algorithm,
    });
  } catch (error) {
    return null;
  }
};

// const token = encryptToken({id: 5})
// console.log('token',token)
// const decoded = decryptToken(token)
// console.log('decoded',decoded)
// console.log(decryptToken(token))
module.exports = {
  encryptToken,
  decryptToken,
};
