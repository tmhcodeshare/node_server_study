const path = require("path");
const fs = require("fs");
const { merge } = require("./mergeHelper");
const {
  // isMainThread, // 是否是主线程
  parentPort, // 用于与父线程通信的端口
  workerData, // 获取线程启动时传递的数据
  // threadId, // 获取线程的唯一编号
} = require("worker_threads");

merge(workerData, (msg) => {
  // console.log("mergeWorker", msg);
  parentPort.postMessage(msg);
});
