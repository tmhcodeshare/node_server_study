require("./Admin");
require("./Class");
require("./Student");
require("./Book");
const sequelize = require("./db");
(async () => {
  try {
    // 同步数据库，同步表结构（根据模型创建表）
    await sequelize.sync({ alert: true });
    console.log("Sync successfully");
  } catch (error) {
    console.log("Sync failed", error);
  }
})();
