const { DataTypes } = require("sequelize");
const sequelize = require("./db");
const moment = require("moment");
const Student = sequelize.define(
  "Student",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    birthday: {
      type: DataTypes.DATE,
      allowNull: false,
      get() { // 获取器getter
        const time = this.getDataValue("birthday");
        // console.log("get birthday", time);
        return moment(time).format("YYYY-MM-DD hh:mm:ss");
      }
    },
    age: { // 虚拟字段，配合访问器可以充当计算属性
      type: DataTypes.VIRTUAL,
      get() {
        const now = moment.utc();
        const birth = moment.utc(this.birthday);
        return now.diff(birth, "y"); //得到两个日期的年份的差异
      }
    },
    sex: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    tel: {
      type: DataTypes.STRING(11),
      allowNull: false,
    },
    classId: {
      type: DataTypes.INTEGER
    }
  },
  {
    tableName: "Student",
    // timestamps: false,
    paranoid: true,
  }
);

module.exports = Student;
