const { DataTypes } = require("sequelize");
const sequelize = require("./db");
const Class = sequelize.define(
  "Class",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    openDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    tableName: "Class",
    // timestamps: false,
    paranoid: true,
  }
);
module.exports = Class;
