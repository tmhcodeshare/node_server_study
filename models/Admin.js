const { DataTypes, Model } = require("sequelize");
const sequelize = require('./db')
// Model 定义模型
class Admin extends Model {}

Admin.init({
  name: {
    type: DataTypes.STRING,
    allowNull: false
  }, 
  account: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: "Admin", // 模型名称
  // timestamps: false,
  // createdAt: false,
  // updatedAt: false,
  paranoid: true // 启用软删除
})


module.exports = Admin;