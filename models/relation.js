const Class = require('./Class');
const Student = require('./Student');
// 建立外键链接
Class.hasMany(Student, { foreignKey: "classId" });
Student.belongsTo(Class, { foreignKey: "classId" });