const { Sequelize } = require("sequelize");
const { sqlLogger } = require("../logger.js");
// 建立一个ORM实例
const sequelize = new Sequelize("sequelize_study", "root", "gahui950110", {
  host: "localhost",
  dialect: "mysql",
  logging: (msg) => {
    // console.log('msg',msg);
    sqlLogger.info(msg);
  },
});

async function connection() {
  try {
    // 连接数据库
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}

connection();

module.exports = sequelize;
